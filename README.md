# rss command line hub

version 0.3.2 (build 2020-04-07/b1)

distributed under the terms of the GNU GPLv3+

## USAGE

```
  rss <COMMAND> [<ARGS>...]
```

## COMMANDS

```
  get <URL>            get and show feed regardless of subscription
  init                 create data files in $HOME/.rsshub
  list                 list feeds
  add <TAG> <URL>      subscribe to feed
  retag <OLD> <NEW>    change feed tag
  rename <TAG> <TITLE> change feed title
  relocate <TAG> <URL> change feed address
  remove <TAGS>...     remove feeds
  remove --dead        remove feeds that failed a previous sync
  sync                 check status of feeds
  show <TAGS>...       show active feed items
  show --all           show active items of all feeds
  show --pinned        show only pinned items
  discard <TAGS>..     discard feed's active items unless pinned
  discard --removed    discard items of removed feeds
  discard --all        discard all items that aren't pinned
  restore <TAGS>...    restore items of selected feeds on next fetch
  restore --all        restore all items on next fetch
  fetch <TAGS>...      get feeds and mark new items active
  fetch --all          get all feeds and mark new items as active
  refetch <TAGS>...    combines restore and fetch into single action
  refetch --all        combines restore and fetch into single action
  pull <TAGS>...       combines discard, fetch and show into single action
  pull --all           combines discard, fetch and show into single action
  pin <TAG> <URL>      pin item
  unpin <TAG> <URL>    unpin item
  unpin --removed      unpin items of removed feeds
  unpin --all          unpin all pinned items
```
