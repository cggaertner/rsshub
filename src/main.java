package org.bitbucket.cggaertner.app.rsshub;

import static org.bitbucket.cggaertner.lib.objects.*;
import static org.bitbucket.cggaertner.lib.stdio.*;
import static org.bitbucket.cggaertner.lib.system.*;

import static java.lang.System.getenv;

import java.util.stream.*;

class main {
    static final String VERSION = "0.3.2";
    static final String BUILD = "2020-04-07/b1";

    static Hub hub = new Hub(homepath(".rsshub"), colors());

    static AnsiColors colors() {
        var tagColor = getenv("RSSHUB_COLOR_TAG");
        var uriColor = getenv("RSSHUB_COLOR_URI");
        var titleColor = getenv("RSSHUB_COLOR_TITLE");
        var okColor = getenv("RSSHUB_COLOR_OK");
        var errorColor = getenv("RSSHUB_COLOR_ERROR");

        if(any(tagColor, uriColor, titleColor, errorColor)) {
            var colors = new AnsiColors();
            colors.setAlias("tag", tagColor);
            colors.setAlias("uri", uriColor);
            colors.setAlias("title", titleColor);
            colors.setAlias("ok", okColor);
            colors.setAlias("error", errorColor);
            return colors;
        }

        return AnsiColors.NONE;
    }

    static void panic(Object... msg) {
        hub.fail(msg);
        die();
    }

    static void argcheck(String[] args, int n) {
        if(args.length != n)
            panic(message.illegalArgs(args.length, n));
    }

    static String programName(String fallback) {
        var name = getenv("RSSHUB_PROGRAM_NAME");
        return name != null ? name : fallback;
    }

    public static void main(String[] cmd) {
        if(cmd.length == 0)
            bye(message.blurb(VERSION, BUILD));

        switch(cmd[0]) {
            case "--help":
            case "init":
            case "list":
            case "add":
            case "retag":
            case "rename":
            case "relocate":
            case "remove":
            case "sync":
            case "get":
            case "show":
            case "discard":
            case "restore":
            case "fetch":
            case "refetch":
            case "pull":
            case "pin":
            case "unpin":
                break;

            default:
                panic(message.unknownAction(cmd[0]));
        }

        var action = cmd[0] + Stream.of(cmd).skip(1)
            .filter(arg -> arg.startsWith("--"))
            .collect(Collectors.joining());

        var args = Stream.of(cmd).skip(1)
            .filter(arg -> !arg.startsWith("--"))
            .toArray(String[]::new);

        var flags = Stream.of(cmd).skip(1)
            .filter(arg -> arg.startsWith("--"))
            .toArray(String[]::new);

        switch(action) {
            case "--help":
                argcheck(args, 0);
                bye(message.usage(programName("rss")));
                break;

            case "init":
                argcheck(args, 0);
                hub.init();
                break;

            case "get":
                argcheck(args, 1);
                hub.get(args[0]);
                break;

            case "list":
                argcheck(args, 0);
                hub.list();
                break;

            case "add":
                argcheck(args, 2);
                hub.add(args[0], args[1]);
                break;

            case "retag":
                argcheck(args, 2);
                hub.retag(args[0], args[1]);
                break;

            case "rename":
                argcheck(args, 2);
                hub.rename(args[0], args[1]);
                break;

            case "relocate":
                argcheck(args, 2);
                hub.relocate(args[0], args[1]);
                break;

            case "remove":
                hub.remove(args);
                break;

            case "remove--dead":
                argcheck(args, 0);
                hub.removeDead();
                break;

            case "sync":
                argcheck(args, 0);
                hub.sync();
                break;

            case "show":
                hub.show(args);
                break;

            case "show--all":
                argcheck(args, 0);
                hub.showAll();
                break;

            case "show--pinned":
                argcheck(args, 0);
                hub.showPinned();
                break;

            case "discard":
                hub.discard(args);
                break;

            case "discard--removed":
                argcheck(args, 0);
                hub.discardRemoved();
                break;

            case "discard--all":
                argcheck(args, 0);
                hub.discardAll();
                break;

            case "restore":
                hub.restore(args);
                break;

            case "restore--all":
                argcheck(args, 0);
                hub.restoreAll();
                break;

            case "fetch":
                hub.fetch(args);
                break;

            case "fetch--all":
                argcheck(args, 0);
                hub.fetchAll();
                break;

            case "refetch":
                hub.restore(args);
                hub.fetch(args);
                break;

            case "refetch--all":
                argcheck(args, 0);
                hub.restoreAll();
                hub.fetchAll();
                break;

            case "pull":
                hub.discard(args);
                hub.fetch(args);
                hub.show(args);
                break;

            case "pull--all":
                argcheck(args, 0);
                hub.discardAll();
                hub.fetchAll();
                hub.showAll();
                break;

            case "pin":
                argcheck(args, 2);
                hub.pin(args[0], args[1]);
                break;

            case "unpin":
                argcheck(args, 2);
                hub.unpin(args[0], args[1]);
                break;

            case "unpin--removed":
                argcheck(args, 0);
                hub.unpinRemoved();
                break;

            case "unpin--all":
                argcheck(args, 0);
                hub.unpinAll();
                break;

            default:
                panic(message.illegalFlag(String.join(" ", flags)));
        }

        if(hub.hasFailed())
            die();
    }
}
