package org.bitbucket.cggaertner.app.rsshub;

import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class AnsiColors {
    private static final Map<String, String> DEFAULT_COLORS;

    static {
        DEFAULT_COLORS = new LinkedHashMap<String, String>();
        DEFAULT_COLORS.put("black", "30");
        DEFAULT_COLORS.put("darkred", "31");
        DEFAULT_COLORS.put("darkgreen", "32");
        DEFAULT_COLORS.put("darkyellow", "33");
        DEFAULT_COLORS.put("darkblue", "34");
        DEFAULT_COLORS.put("darkmagenta", "35");
        DEFAULT_COLORS.put("darkcyan", "36");
        DEFAULT_COLORS.put("gray", "37");
        DEFAULT_COLORS.put("darkgray", "30;1");
        DEFAULT_COLORS.put("red", "31;1");
        DEFAULT_COLORS.put("green", "32;1");
        DEFAULT_COLORS.put("yellow", "33;1");
        DEFAULT_COLORS.put("blue", "34;1");
        DEFAULT_COLORS.put("magenta", "35;1");
        DEFAULT_COLORS.put("cyan", "36;1");
        DEFAULT_COLORS.put("white", "37;1");
    }

    public static final AnsiColors NONE = new AnsiColors(null) {
        public String colorize(String color, String text) {
            return text;
        }
    };

    private final Map<String, String> colors;

    public AnsiColors(Map<String, String> colors) {
        this.colors = colors;
    }

    public AnsiColors() {
        this(new HashMap<String, String>(DEFAULT_COLORS));
    }

    public void setAlias(String alias, String colorName) {
        if(colorName == null)
            return;

        if(!colors.containsKey(colorName))
            throw new IllegalArgumentException("unknown color " + colorName);

        colors.put(alias, colors.get(colorName));
    }

    public void registerColor(String name, String colorSequence) {
        colors.put(name, colorSequence);
    }

    public String colorize(String color, String text) {
        return colors.containsKey(color)
            ? "\033[" + colors.get(color) + "m" + text + "\033[0m"
            : text;
    }

    public static void main(String[] args) {
        var colors = new AnsiColors();

        for(var color : DEFAULT_COLORS.keySet())
            System.out.println(colors.colorize(color, color));
    }
}
