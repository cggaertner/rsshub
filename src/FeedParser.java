package org.bitbucket.cggaertner.app.rsshub;

import java.io.InputStream;
import java.util.Iterator;
import java.util.NoSuchElementException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class FeedParser implements XMLStreamConstants, AutoCloseable {
    private static enum State {
        IGNORE,
        RSS,
        CHANNEL,
        FEED,
        ITEM,
        ENTRY,
    }

    public static class Item {
        public final String link;
        public final String title;

        public Item(String link, String title) {
            this.link = link;
            this.title = title;
        }

        public Item(String link) {
            this(link, null);
        }

        public boolean hasTitle() {
            return title != null;
        }
    }

    private XMLStreamReader reader;
    private State state;
    private State parentState;
    private String title;
    private String itemTitle;
    private String itemLink;
    private boolean itemReady;
    private int ignorance;
    private XMLStreamException lastIterationFailure;

    public FeedParser(InputStream is) throws XMLStreamException {
        var fact = XMLInputFactory.newInstance();
        fact.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        fact.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, false);
        fact.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES,
                false);

        reader = fact.createXMLStreamReader(is);
        if(reader.nextTag() == START_ELEMENT) {
            switch(reader.getLocalName()) {
                case "rss":
                state = State.RSS;
                break;

                case "feed":
                state = State.FEED;
                break;

                default:
                throw new XMLStreamException(reader.getLocalName()
                    + " is not a recognized feed format");
            }
        }
    }

    public void close() throws XMLStreamException {
        reader.close();
    }

    public boolean hasIterationFailed() {
        return lastIterationFailure != null;
    }

    public XMLStreamException getLastIterationFailure() {
        return lastIterationFailure;
    }

    public boolean seekTitle() throws XMLStreamException {
        while(!parsedTitle() && parse());
        return parsedTitle();
    }

    public boolean seekItem() throws XMLStreamException {
        while(!parsedItem() && parse());
        return parsedItem();
    }

    public boolean parsedTitle() {
        return title != null;
    }

    public String getTitle() {
        return title;
    }

    public boolean parsedItem() {
        return itemReady;
    }

    public Item getItem() {
        return parsedItem() ? new Item(itemLink, itemTitle) : null;
    }

    public void discardItem() {
        itemTitle = null;
        itemLink = null;
        itemReady = false;
    }

    public Item takeItem() {
        try {
            return getItem();
        }
        finally {
            discardItem();
        }
    }

    private boolean hasPrefix() {
        var prefix = reader.getPrefix();
        return prefix != null && prefix.length() > 0;
    }

    private void ignore() {
        if(state != State.IGNORE) {
            parentState = state;
            state = State.IGNORE;
        }

        ++ignorance;
    }

    private void unignore() {
        --ignorance;
        if(ignorance == 0) {
            state = parentState;
            parentState = null;
        }
    }

    public Iterable<FeedParser.Item> items() {
        return () -> new Iterator<Item>() {
            public boolean hasNext() {
                try {
                    return seekItem();
                }
                catch(XMLStreamException e) {
                    lastIterationFailure = e;
                    return false;
                }
            }

            public Item next() {
                if(hasNext())
                    return takeItem();

                throw new NoSuchElementException();
            }
        };
    }

    public boolean parse() throws XMLStreamException {
        if(!reader.hasNext())
            return false;

        int event;
        try { event = reader.next(); }
        catch(NullPointerException e) {
            throw new XMLStreamException(e);
        }

        switch(state) {
            case IGNORE:
            switch(event) {
                case START_ELEMENT:
                ignore();
                break;

                case END_ELEMENT:
                unignore();
                break;
            }
            break;

            case RSS:
            if(event == START_ELEMENT) {
                switch(reader.getLocalName()) {
                    case "channel":
                    state = State.CHANNEL;
                    break;

                    default:
                    ignore();
                }
            }
            break;

            case CHANNEL:
            if(event == START_ELEMENT) {
                switch(reader.getLocalName()) {
                    case "title":
                    if(hasPrefix() || title != null) ignore();
                    else title = reader.getElementText();
                    break;

                    case "item":
                    if(hasPrefix()) ignore();
                    else state = State.ITEM;
                    break;

                    default:
                    ignore();
                }
            }
            break;

            case FEED:
            if(event == START_ELEMENT) {
                switch(reader.getLocalName()) {
                    case "title":
                    if(hasPrefix() || title != null) ignore();
                    else title = reader.getElementText();
                    break;

                    case "entry":
                    if(hasPrefix()) ignore();
                    else state = State.ENTRY;
                    break;

                    default:
                    ignore();
                }
            }
            break;

            case ITEM:
            switch(event) {
                case END_ELEMENT:
                if(hasPrefix() || !"item".equals(reader.getLocalName()))
                    throw new XMLStreamException("nesting error");

                itemReady = true;
                state = State.CHANNEL;
                break;

                case START_ELEMENT:
                switch(reader.getLocalName()) {
                    case "title":
                    if(hasPrefix() || itemTitle != null) ignore();
                    else itemTitle = reader.getElementText();
                    break;

                    case "link":
                    if(hasPrefix() || itemLink != null) ignore();
                    else itemLink = reader.getElementText();
                    break;

                    default:
                    ignore();
                }
                break;
            }
            break;

            case ENTRY:
            switch(event) {
                case END_ELEMENT:
                if(hasPrefix() || !"entry".equals(reader.getLocalName()))
                    throw new XMLStreamException("nesting error");

                itemReady = true;
                state = State.FEED;
                break;

                case START_ELEMENT:
                switch(reader.getLocalName()) {
                    case "title":
                    if(hasPrefix() || itemTitle != null) ignore();
                    else itemTitle = reader.getElementText();
                    break;

                    case "link":
                    if(!hasPrefix() && itemLink== null) {
                        var href = reader.getAttributeValue(null, "href");
                        var rel = reader.getAttributeValue(null, "rel");
                        var type = reader.getAttributeValue(null, "type");

                        var valid_href = href != null;
                        var valid_rel = rel == null || rel.equals("alternate");
                        var valid_type = type == null
                                || type.equals("text/html");

                        if(valid_href && valid_rel && valid_type)
                            itemLink = href;
                    }

                    ignore();
                    break;

                    default:
                    ignore();
                }
                break;
            }
            break;
        }

        return true;
    }
}
