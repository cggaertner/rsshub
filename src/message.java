package org.bitbucket.cggaertner.app.rsshub;

import static java.lang.String.format;

class message {
    static final String BLURB =
        "rss command line hub\n"
      + "version %s (build %s)\n"
      + "\n"
      + "distributed under the terms of the GNU GPLv3+\n"
      + "check `--help` for usage details"
      ;

    static final String USAGE =
        "USAGE\n"
      + "  %s <COMMAND> [<ARGS>...]"
      + "\n\n"
      + "COMMANDS\n"
      + "  get <URL>            get and show feed regardless of subscription\n"
      + "  init                 create data files in $HOME/.rsshub\n"
      + "  list                 list feeds\n"
      + "  add <TAG> <URL>      subscribe to feed\n"
      + "  retag <OLD> <NEW>    change feed tag\n"
      + "  rename <TAG> <TITLE> change feed title\n"
      + "  relocate <TAG> <URL> change feed address\n"
      + "  remove <TAGS>...     remove feeds\n"
      + "  remove --dead        remove feeds that failed a previous sync\n"
      + "  sync                 check status of feeds\n"
      + "  show <TAGS>...       show active feed items\n"
      + "  show --all           show active items of all feeds\n"
      + "  show --pinned        show only pinned items\n"
      + "  discard <TAGS>..     discard feed's active items unless pinned\n"
      + "  discard --removed    discard items of removed feeds\n"
      + "  discard --all        discard all items that aren't pinned\n"
      + "  restore <TAGS>...    restore items of selected feeds on next fetch\n"
      + "  restore --all        restore all items on next fetch\n"
      + "  fetch <TAGS>...      get feeds and mark new items active\n"
      + "  fetch --all          get all feeds and mark new items as active\n"
      + "  refetch <TAGS>...    combines restore and fetch into single action\n"
      + "  refetch --all        combines restore and fetch into single action\n"
      + "  pull <TAGS>...       combines discard, fetch and show "
                             + "into single action\n"
      + "  pull --all           combines discard, fetch and show "
                             + "into single action\n"
      + "  pin <TAG> <URL>      pin item\n"
      + "  unpin <TAG> <URL>    unpin item\n"
      + "  unpin --removed      unpin items of removed feeds\n"
      + "  unpin --all          unpin all pinned items\n"
      ;

    static final String ILLEGAL_ARGS =
        "illegal argument count: expected %d, got %d";

    static final String ILLEGAL_FLAG =
        "illegal flag `%s`";

    static final String ILLEGAL_URI =
        "illegal URI `%s`: %s";

    static final String UNKNOWN_ITEM =
        "no item `%s` in feed `%s`";

    static final String FAILED_CREATION =
        "failed to create %s: %s";

    static final String FAILED_READ =
        "failed to read from %s: %s";

    static final String FAILED_WRITE =
        "failed to write to %s: %s";

    static final String FAILED_FETCH =
        "failed to fetch %s: %s";

    static final String TIMED_OUT =
        "fetching %s has timed out";

    static final String CORRUPT_DATA =
        "encountered corrupt data while reading %s";

    static final String CORRUPT_FEED =
        "feed `%s` is corrupt: %s";

    static final String UNKNOWN_ACTION =
        "action `%s` is not known";

    static final String UNKNOWN_FEED =
        "feed `%s` is not known";

    static final String EXISTING_TAG =
        "tag `%s` is already in use";

    static final String EXISTING_FEED =
        "feed `%s` has already been added as `%s`";

    static final String NOT_PINNED =
        "item `%s` of feed `%s` is not pinned";

    static final String ALREADY_PINNED =
        "item `%s` of feed `%s` is already pinned";

    static String blurb(String version, String build) {
        return format(BLURB, version, build);
    }

    static String usage(String name) {
        return format(USAGE, name);
    }

    static String illegalArgs(int count, int n) {
        return format(ILLEGAL_ARGS, n, count);
    }

    static String illegalFlag(String flag) {
        return format(ILLEGAL_FLAG, flag);
    }

    static String illegalUri(String uri, String msg) {
        return format(ILLEGAL_URI, uri, msg);
    }

    static String failedCreation(Object target, String msg) {
        return format(FAILED_CREATION, target, msg);
    }

    static String failedRead(Object target, String msg) {
        return format(FAILED_READ, target, msg);
    }

    static String failedWrite(Object target, String msg) {
        return format(FAILED_WRITE, target, msg);
    }

    static String failedFetch(Object uri, String msg) {
        return format(FAILED_FETCH, uri, msg);
    }

    static String unknownItem(String tag, String uri) {
        return format(UNKNOWN_ITEM, uri, tag);
    }

    static String unknownFeed(String feed) {
        return format(UNKNOWN_FEED, feed);
    }

    static String unknownAction(String action) {
        return format(UNKNOWN_ACTION, action);
    }

    static String corruptData(Object target) {
        return format(CORRUPT_DATA, target);
    }

    static String corruptFeed(String uri, String msg) {
        return format(CORRUPT_FEED, uri, msg);
    }

    static String existingTag(String tag) {
        return format(EXISTING_TAG, tag);
    }

    static String existingFeed(String uri, String tag) {
        return format(EXISTING_FEED, uri, tag);
    }

    static String notPinned(String tag, String uri) {
        return format(NOT_PINNED, uri, tag);
    }

    static String alreadyPinned(String tag, String uri) {
        return format(ALREADY_PINNED, uri, tag);
    }

    static String timedOut(Object uri) {
        return format(TIMED_OUT, uri);
    }
}
