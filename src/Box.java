package org.bitbucket.cggaertner.app.rsshub;

import java.util.function.Function;

class Box<L,V> {
    public static <L,V> Box<L,V> pack(L label, V value) {
        return new Box<L,V>(label, value);
    }

    public final L label;
    public final V value;

    public Box(L label, V value) {
        this.label = label;
        this.value = value;
    }

    public <W> Box<L,W> map(Function<V,W> fn) {
        return new Box<L,W>(label, fn.apply(value));
    }
}
