package org.bitbucket.cggaertner.app.rsshub;

class SyncStatus {
    static enum Code { OK, RENAMED, DEAD }

    public static final SyncStatus OK = new SyncStatus(Code.OK);
    public static final SyncStatus DEAD = new SyncStatus(Code.DEAD);

    public static SyncStatus renamed(String title) {
        return new SyncStatus(Code.RENAMED, title);
    }

    public final Code code;
    public final String title;

    private SyncStatus(Code code, String title) {
        this.code = code;
        this.title = title;
    }

    private SyncStatus(Code code) {
        this(code, null);
    }

    public boolean isDead() {
        return code == Code.DEAD;
    }
}
