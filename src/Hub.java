package org.bitbucket.cggaertner.app.rsshub;

import org.bitbucket.cggaertner.lib.http;

import static org.bitbucket.cggaertner.lib.filesys.*;
import static org.bitbucket.cggaertner.lib.objects.*;
import static org.bitbucket.cggaertner.lib.stdio.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import javax.xml.stream.XMLStreamException;

class Hub {
    static int TAG_PADDING = 12;
    static long TIMEOUT = 20000;

    static String prepad(String string, int size) {
        int len = string.length();
        return len < size ? " ".repeat(size - len)  + string :
               len > size ? string.substring(0, size - 1) + "…"
                          : string;
    }

    static String fallback(String value, String fallback) {
        return value != null && value.length() > 0 ? value : fallback;
    }

    static Map<String, Triple> byUri(Collection<Triple> triples) {
        return triples.stream().collect(
            HashMap<String, Triple>::new,
            (map, triple) -> map.put(triple.uri, triple),
            Map::putAll
        );
    }

    class Pathset {
        final Path root;
        final Path feeds;
        final Path items;
        final Path cache;
        final Path pinboard;
        final Path graveyard;

        Pathset(Path root) {
            this.root = root;
            feeds = root.resolve("feeds");
            items = root.resolve("items");
            cache = root.resolve("cache");
            pinboard = root.resolve("pinboard");
            graveyard = root.resolve("graveyard");
        }
    }

    Pathset paths;
    AnsiColors colors;
    boolean failed;

    Map<String,Triple> feedMap;
    Collection<Triple> feedCollection;
    Set<String> tagSet;

    List<Triple> itemList;
    Set<String> itemSet;

    Set<String> cacheSet, pinSet, graveSet;

    Hub(Path root, AnsiColors colors) {
        this.paths = new Pathset(root);
        this.colors = colors;
    }

    boolean hasFailed() {
        return failed;
    }

    boolean loadFeeds() {
        var path = paths.feeds;
        try(var lines = Files.lines(path)) {
            feedMap = lines.map(Triple::parse).collect(
                LinkedHashMap<String,Triple>::new,
                (map, triple) -> map.put(triple.tag, triple),
                Map::putAll);

            feedCollection = feedMap.values();
            tagSet = feedMap.keySet();
            return true;
        }
        catch(IOException e) {
            fail(message.failedRead(path, classname(e)));
            return false;
        }
        catch(IllegalArgumentException e) {
            fail(message.corruptData(path));
            return false;
        }
    }

    boolean storeFeeds() {
        return writeLines(paths.feeds,
            StreamSupport.stream(feeds().spliterator(), false)
                .map(Triple::toString));
    }

    boolean loadItems() {
        var path = paths.items;
        try(var lines = Files.lines(path)) {
            itemList = lines
                .map(Triple::parse)
                .collect(Collectors.toList());

            itemSet = itemList.stream()
                .map(Triple::id)
                .collect(Collectors.toSet());

            return true;
        }
        catch(IOException e) {
            fail(message.failedRead(path, classname(e)));
            return false;
        }
        catch(IllegalArgumentException e) {
            fail(message.corruptData(path));
            return false;
        }
    }

    Set<String> readSet(Path path) {
        try(var lines = Files.lines(path)) {
            return lines.collect(Collectors.toSet());
        }
        catch(IOException e) {
            fail(message.failedRead(path, classname(e)));
            return null;
        }
    }

    boolean writeLines(Path path, Collection<String> lines) {
        try {
            Files.write(path, lines);
            return true;
        }
        catch(IOException e) {
            fail(message.failedWrite(path, classname(e)));
            return false;
        }
    }

    boolean writeLines(Path path, Stream<String> lines) {
        try {
            Files.write(path, (Iterable<String>)lines::iterator);
            return true;
        }
        catch(IOException e) {
            fail(message.failedWrite(path, classname(e)));
            return false;
        }
    }

    boolean clearFile(Path path) {
        try {
            truncate(path, 0);
            return true;
        }
        catch(IOException e) {
            fail(message.failedWrite(path, classname(e)));
            return false;
        }
    }

    boolean loadCache() {
        return (cacheSet = readSet(paths.cache)) != null;
    }

    boolean loadPins() {
        return (pinSet = readSet(paths.pinboard)) != null;
    }

    boolean loadGraves() {
        return (graveSet = readSet(paths.graveyard)) != null;
    }

    Collection<Triple> feeds() {
        return feedCollection;
    }

    Triple feed(String tag) {
        return feedMap.get(tag);
    }

    Collection<Triple> items() {
        return itemList;
    }

    boolean isCached(Triple item) {
        return cacheSet.contains(item.id());
    }

    boolean isPinned(Triple item) {
        return pinSet.contains(item.id());
    }

    boolean isPinned(String tag, String uri) {
        return pinSet.contains(Triple.id(tag, uri));
    }

    boolean isDead(Triple feed) {
        return graveSet.contains(feed.uri);
    }

    String colorize(String color, Object... tokens) {
        return colors.colorize(color, Stream.of(tokens)
            .map(Object::toString)
            .collect(Collectors.joining()));
    }

    String colorize(FeedParser.Item item) {
        return colors.colorize("uri", item.link) + " "
             + colors.colorize("title", fallback(item.title, "?"));
    }

    synchronized void fail(Object... msg) {
        cry(colorize("error", msg));
        failed = true;
    }

    synchronized void echo(Object... msg) {
        say(msg);
    }

    String responseMessage(http.Response response) {
        var status = response.status;
        return status == 0 ? classname(response.failure) : status + "";
    }

    boolean checkResponse(http.Response response, boolean quiet) {
        var status = response.status;
        if(status == 200)
            return true;

        if(!quiet) {
            fail(message.failedFetch(response.requestUri,
                responseMessage(response)));
        }

        return false;
    }

    boolean checkResponse(http.Response response) {
        return checkResponse(response, false);
    }

    void init() {
        try {
            makepath(paths.root);
        }
        catch(IOException e) {
            fail(message.failedCreation(paths.root, classname(e)));
            return;
        }

        Stream.of("feeds", "items", "cache", "pinboard", "graveyard")
            .map(paths.root::resolve)
            .forEach(path -> {
                try {
                    createfile(path, 0600);
                }
                catch(IOException e) {
                    fail(message.failedCreation(path, classname(e)));
                }
            });
    }

    void list() {
        if(!loadFeeds()) return;

        for(var feed : feeds()) {
            say(colorize("tag", prepad(feed.tag, TAG_PADDING)), " ",
                colorize("uri", feed.uri), " ",
                colorize("title", feed.fallbackTitle()));
        }
    }

    void get(String uri) {
        try {
            var response = http.client().get(uri).await();
            if(!checkResponse(response)) return;

            try(var body = response.body;
                var parser = new FeedParser(body)) {
                for(var item : parser.items())
                    say(colorize(item));

                if(parser.hasIterationFailed()) {
                    throw new XMLStreamException(
                        parser.getLastIterationFailure().getMessage());
                }
            }
        }
        catch(IllegalArgumentException e) {
            fail(message.illegalUri(uri, e.getMessage()));
        }
        catch(IOException e) {
            fail(message.failedFetch(uri, classname(e)));
        }
        catch(XMLStreamException e) {
            fail(message.corruptFeed(uri, e.getMessage()));
        }
    }

    void pin(String tag, String uri) {
        if(!(loadFeeds() && loadItems() && loadPins())) return;

        if(!tagSet.contains(tag)) {
            fail(message.unknownFeed(tag));
            return;
        }

        var id = Triple.id(tag, uri);
        if(!itemSet.contains(id)) {
            fail(message.unknownItem(tag, uri));
            return;
        }

        if(isPinned(tag, uri)) {
            fail(message.alreadyPinned(tag, uri));
            return;
        }

        try {
            Files.writeString(paths.pinboard, id + "\n",
                StandardOpenOption.APPEND);
        }
        catch(IOException e) {
            fail(message.failedWrite(paths.pinboard, classname(e)));
        }
    }

    void unpin(String tag, String uri) {
        if(!loadPins()) return;

        if(!isPinned(tag, uri)) {
            fail(message.notPinned(tag, uri));
            return;
        }

        pinSet.remove(Triple.id(tag, uri));
        writeLines(paths.pinboard, pinSet);
    }

    void unpinRemoved() {
        if(!(loadFeeds() && loadPins())) return;

        boolean modified = false;

        var newPinboard = new ArrayList<String>();
        for(var id : pinSet) {
            if(tagSet.contains(Triple.tagFromId(id)))
                newPinboard.add(id);
            else modified = true;
        }

        if(modified) try {
            Files.write(paths.pinboard, newPinboard);
        }
        catch(IOException e) {
            fail(message.failedWrite(paths.pinboard, classname(e)));
        }
    }

    void unpinAll() {
        clearFile(paths.pinboard);
    }

    private void show(Set<String> tags, Set<String> ids) {
        var map = new HashMap<String, List<Triple>>();
        for(var tag : tags) {
            map.put(tag, new ArrayList<Triple>());
        }

        for(var item : items()) {
            if(tags.contains(item.tag) && ids.contains(item.id()))
                map.get(item.tag).add(item);
        }

        for(var tag : tags) {
            var list = map.get(tag);
            if(list.isEmpty()) continue;

            var feed = feed(tag);
            say(colorize("tag", tag), " ",
                colorize("title", feed.fallbackTitle()));

            for(var item : list) {
                say("  ", colorize("uri", item.uri), " ",
                    colorize("title", item.fallbackTitle()),
                    isPinned(item) ? " [pinned]" : "");
            }
        }
    }

    void show(String... tags) {
        if(!(loadFeeds() && loadItems() && loadPins())) return;
        show(new LinkedHashSet<String>(Arrays.asList(tags)), itemSet);
    }

    void showAll() {
        if(!(loadFeeds() && loadItems() && loadPins())) return;
        show(tagSet, itemSet);
    }

    void showPinned() {
        if(!(loadFeeds() && loadItems() && loadPins())) return;
        show(tagSet, pinSet);
    }

    void rename(String tag, String title) {
        if(!loadFeeds()) return;

        if(!tagSet.contains(tag)) {
            fail(message.unknownFeed(tag));
            return;
        }

        feed(tag).rename(title);
        storeFeeds();
    }

    void relocate(String tag, String uri) {
        if(!loadFeeds()) return;

        if(!tagSet.contains(tag)) {
            fail(message.unknownFeed(tag));
            return;
        }

        feed(tag).relocate(uri);
        storeFeeds();
    }

    void remove(String... tags) {
        if(!loadFeeds()) return;

        boolean modified = false;

        for(var tag : tags) {
            if(!tagSet.contains(tag)) {
                fail(message.unknownFeed(tag));
                continue;
            }

            feedMap.remove(tag);
            modified = true;
        }

        if(modified)
            storeFeeds();
    }

    void removeDead() {
        if(!(loadFeeds() && loadGraves())) return;

        boolean modified = false;

        for(var feed : feeds()) {
            if(!graveSet.contains(feed.uri))
                continue;

            feedMap.remove(feed.tag);
            modified = true;
        }

        if(modified && !storeFeeds())
            return;

        clearFile(paths.graveyard);
    }

    void add(String tag, String uri) {
        if(!loadFeeds()) return;

        if(tagSet.contains(tag)) {
            fail(message.existingTag(tag));
            return;
        }

        var otherFeed = byUri(feeds()).get(uri);
        if(otherFeed != null) {
            fail(message.existingFeed(uri, otherFeed.tag));
            return;
        }

        Triple feed = null;

        try {
            var response = http.client().get(uri).await();
            if(!checkResponse(response)) return;

            try(var body = response.body;
                var parser = new FeedParser(body)) {

                parser.seekTitle();
                feed = new Triple(tag, uri, parser.getTitle());
            }
        }
        catch(IllegalArgumentException e) {
            fail(message.illegalUri(uri, e.getMessage()));
        }
        catch(IOException e) {
            fail(message.failedFetch(uri, classname(e)));
        }
        catch(XMLStreamException e) {
            fail(message.corruptFeed(uri, e.getMessage()));
        }

        say(colorize("title", feed.fallbackTitle()));
        feedMap.put(tag, feed);
        storeFeeds();
    }

    void sync() {
        if(!loadFeeds()) return;

        var client = http.client();
        var graveyard = feeds().stream()
            .map(feed -> Box.pack(feed, client.get(feed.uri)))
            .parallel()
            .map(box -> box.map(asyncResponse -> {
                var response = asyncResponse.await();

                SyncStatus status = null;
                String msg = null;

                if(!checkResponse(response, true)) {
                    status = SyncStatus.DEAD;
                    msg = responseMessage(response);
                }
                else try(var body = response.body;
                    var parser = new FeedParser(body)) {
                    parser.seekTitle();

                    var newTitle = parser.getTitle();
                    var oldTitle = box.label.title;

                    status = eq(newTitle, oldTitle)
                        ? SyncStatus.OK
                        : SyncStatus.renamed(newTitle);
                }
                catch(XMLStreamException | IOException e) {
                    status = SyncStatus.DEAD;
                    msg = classname(e);
                }

                var feed = box.label;
                switch(status.code) {
                    case OK:
                    echo(colorize("tag", prepad(feed.tag, TAG_PADDING)), " ",
                         colorize("ok", "ok"));
                    break;

                    case RENAMED:
                    echo(colorize("tag", prepad(feed.tag, TAG_PADDING)), " ",
                         colorize("title", feed.fallbackTitle()), " → ",
                         colorize("title", fallback(status.title, "?")));
                    break;

                    case DEAD:
                    echo(colorize("tag", prepad(feed.tag, TAG_PADDING)), " ",
                         colorize("error", "dead: " + msg));
                    break;
                }

                return status;
            }))
            .filter(box -> box.value.isDead())
            .collect(
                ArrayList<String>::new,
                (list, box) -> list.add(box.label.uri),
                List::addAll
            );

        try {
            Files.write(paths.graveyard, graveyard);
        }
        catch(IOException e) {
            fail(message.failedWrite(paths.graveyard, classname(e)));
        }
    }

    private void fetch(Set<String> tags) {
        var newCache = Collections.synchronizedSet(tags == tagSet
            ? new HashSet<String>()
            : cacheSet.stream()
                .filter(id -> !tags.contains(Triple.tagFromId(id)))
                .collect(HashSet<String>::new, Set::add, Set::addAll));

        var failedFeeds = Collections.synchronizedSet(new HashSet<String>());

        var client = http.client();
        var lines = tags.stream()
            .map(this::feed)
            .map(feed -> Box.pack(feed, client.get(feed.uri)))
            .parallel()
            .map(box -> box.map(http.AsyncResponse::await))
            .filter(box -> {
                var response = box.value;
                var status = response.status;
                if(status == 200)
                    return true;

                fail(message.failedFetch(response.requestUri,
                    responseMessage(response)));

                failedFeeds.add(box.label.tag);

                return false;
            })
            .map(box -> {
                var feed = box.label;
                var response = box.value;

                var newItems = new ArrayList<String>();
                Throwable failure = null;

                var done = new Flag();
                var timer = new Thread(() -> {
                    try {
                        Thread.sleep(TIMEOUT);
                        if(!done.isSet()) {
                            fail(message.timedOut(response.requestUri));
                            response.body.close();
                        }
                    }
                    catch(InterruptedException e) {}
                    catch(IOException e) {}
                });

                timer.setDaemon(true);
                timer.start();

                try(var body = response.body;
                    var parser = new FeedParser(body)) {

                    for(var item : parser.items()) {
                        var triple = new Triple(feed.tag,
                            item.link, item.title);

                        newCache.add(triple.id());

                        if(!cacheSet.contains(triple.id())
                                && !itemSet.contains(triple.id()))
                            newItems.add(triple.toString());
                    }

                    failure = parser.getLastIterationFailure();
                }
                catch(XMLStreamException | IOException e) {
                    failure = e;
                }
                finally {
                    done.set();
                }

                if(failure != null) {
                    fail(message.failedFetch(feed.uri, classname(failure)));
                    failedFeeds.add(feed.tag);
                }

                return newItems;
            })
            .collect(ArrayList<String>::new, List::addAll, List::addAll);

        // add cache entries of failed feeds to avoid item restoration
        // on next fetch
        for(var id : cacheSet) {
            if(failedFeeds.contains(Triple.tagFromId(id)))
                newCache.add(id);
        }


        writeLines(paths.cache, newCache);

        try {
            Files.write(paths.items, lines, StandardOpenOption.APPEND);
        }
        catch(IOException e) {
            fail(message.failedWrite(paths.items, classname(e)));
        }
    }

    void fetchAll() {
        if(!(loadFeeds() && loadItems() && loadCache())) return;
        fetch(tagSet);
    }

    void fetch(String... tags) {
        if(!(loadFeeds() && loadItems() && loadCache())) return;
        fetch(new HashSet<String>(Arrays.asList(tags)));
    }

    void restoreAll() {
        clearFile(paths.cache);
    }

    void restore(String... tags) {
        if(!(loadFeeds() && loadCache())) return;

        var set = new HashSet<String>(Arrays.asList(tags));
        for(var tag : tags) {
            if(!tagSet.contains(tag)) {
                set.remove(tag);
                fail(message.unknownFeed(tag));
            }
        }

        var newCache = cacheSet.stream()
            .filter(id -> !set.contains(Triple.tagFromId(id)))
            .collect(Collectors.toSet());

        writeLines(paths.cache, newCache);
    }

    void discard(String... tags) {
        if(!(loadFeeds() && loadItems() && loadPins())) return;

        var set = new HashSet<String>(Arrays.asList(tags));
        for(var tag : tags) {
            if(!tagSet.contains(tag)) {
                set.remove(tag);
                fail(message.unknownFeed(tag));
            }
        }

        writeLines(paths.items, items().stream()
            .filter(item -> isPinned(item) || !set.contains(item.tag))
            .map(Triple::toString));
    }

    void discardAll() {
        if(!(loadItems() && loadPins())) return;

        writeLines(paths.items, items().stream()
            .filter(this::isPinned)
            .map(Triple::toString));
    }

    void discardRemoved() {
        if(!(loadFeeds() && loadItems())) return;

        boolean modified = false;

        var newItems = new ArrayList<String>();
        for(var item : items()) {
            if(tagSet.contains(item.tag))
                newItems.add(item.toString());
            else modified = true;
        }

        if(modified) try {
            Files.write(paths.items, newItems);
        }
        catch(IOException e) {
            fail(message.failedWrite(paths.items, classname(e)));
        }
    }

    void retag(String oldTag, String newTag) {
        if(!(loadFeeds() && loadItems() && loadCache() && loadPins())) return;

        var flag = new Flag();

        var feed = feed(oldTag);
        if(feed != null) {
            feed.tag = newTag;
            flag.set();
        }

        for(var item : items()) {
            if(oldTag.equals(item.tag)) {
                item.tag = newTag;
                flag.set();
            }
        }

        var newCache = cacheSet.stream().map(id -> {
            var tag = Triple.tagFromId(id);
            var newId = id;
            if(oldTag.equals(tag)) {
                newId = Triple.id(newTag, Triple.uriFromId(id));
                flag.set();
            }

            return newId;
        }).collect(Collectors.toList());

        var newPins = pinSet.stream().map(id -> {
            var tag = Triple.tagFromId(id);
            var newId = id;
            if(oldTag.equals(tag)) {
                newId = Triple.id(newTag, Triple.uriFromId(id));
                flag.set();
            }

            return newId;
        }).collect(Collectors.toList());

        if(!flag.isSet())
            fail(message.unknownFeed(oldTag));

        storeFeeds();
        writeLines(paths.items, items().stream().map(Triple::toString));
        writeLines(paths.cache, newCache);
        writeLines(paths.pinboard, newPins);
    }
}
