package org.bitbucket.cggaertner.app.rsshub;

class Flag {
    private boolean value;

    public Flag() {}

    public synchronized boolean isSet() {
        return value;
    }

    public synchronized int intValue() {
        return value ? 1 : 0;
    }

    public synchronized void set() {
        value = true;
    }

    public synchronized void unset() {
        value = false;
    }
}
