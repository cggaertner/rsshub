package org.bitbucket.cggaertner.app.rsshub;

class Triple {
    static Triple parse(String line) {
        return new Triple(line.split(" ", 3));
    }

    static String id(String tag, String uri) {
        return tag + "#" + uri;
    }

    static String tagFromId(String id) {
        var pos = id.indexOf('#');
        if(pos < 0) return null;

        return id.substring(0, pos);
    }

    static String uriFromId(String id) {
        var pos = id.indexOf('#');
        if(pos < 0) return null;

        return id.substring(pos + 1);
    }

    String tag, uri, title;

    Triple(String tag, String uri, String title) {
        this.tag = tag;
        this.uri = uri;
        this.title = title;
    }

    Triple(String[] tuple) {
       if(tuple.length < 2 || tuple.length > 3)
            throw new IllegalArgumentException();

        tag = tuple[0];
        uri = tuple[1];
        if(tuple.length == 3)
            title = tuple[2];
    }

    String id() {
        return id(tag, uri);
    }

    void retag(String title) {
        this.title = title;
    }

    void relocate(String uri) {
        this.uri = uri;
    }

    void rename(String title) {
        this.title = title;
    }

    boolean hasTitle() {
        return title != null;
    }

    String fallbackTitle() {
        return title != null && title.length() > 0 ? title : "?";
    }

    public String toString() {
        return tag + " " + uri + (hasTitle() ? " " + title : "");
    }
}
