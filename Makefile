share/java/rsshub/rsshub.jar: $(wildcard lib/*.java src/*.java)
	javac -d . $^
	jar cfe $@ org.bitbucket.cggaertner.app.rsshub.main org

update:
	git submodule update --remote 

realclean:
	rm -f share/java/rsshub/rsshub.jar
